using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandInvoker
{
    private List<ICommand> commandsToDo;
    private List<ICommand> commandsDone;

    public CommandInvoker()
    {
        commandsToDo = new List<ICommand>();
        commandsDone = new List<ICommand>();
    }
    public void AddCommand(ICommand command)
    {
        if (command != null)
        {
            commandsToDo.Add(command);
        }
    }
    public void ProcessAll()
    {
        for (int i = 0; i < commandsToDo.Count; i++)
        {
            commandsToDo[i].Invoke();
            commandsDone.Add(commandsToDo[i]);
        }
        ClearQueue();
    }
    public void Process()
    {
        if (commandsToDo.Count >= 1)
        {
            commandsToDo[0].Invoke();
            commandsDone.Add(commandsToDo[0]);
            commandsToDo.RemoveAt(0);
        }
    }
    public void Undo()
    {
        if (commandsDone.Count > 0)
        {
            commandsDone[commandsDone.Count - 1].Undo();
            commandsDone.RemoveAt(commandsDone.Count - 1);
        }
    }
    public void ClearQueue()
    {
        commandsToDo.Clear();
    }
}
