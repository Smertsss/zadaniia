using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICommand
{
    public void Invoke();
    public void Undo();
}

public class MoveForward : ICommand
{
    private Transform t;
    private float dist;
    public MoveForward(Transform t, float dist)
    {
        this.t = t;
        this.dist = dist;
    }
    public void Invoke()
    {
        t.position = new Vector3(t.position.x + dist, 0, 0);
    }

    public void Undo()
    {
        t.position = new Vector3(t.position.x - dist, 0, 0);
    }
}

public class Rotator : ICommand
{

    private Transform t;
    private float angle;
    public Rotator(Transform t, float angle)
    {
        this.t = t;
        this.angle = angle;
    }
    public void Invoke()
    {
        t.Rotate(0, 0, angle);
    }

    public void Undo()
    {
        t.Rotate(0, 0, -angle);
    }
}