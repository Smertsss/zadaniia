using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandController : MonoBehaviour
{
    CommandInvoker commandInvoker = new CommandInvoker();

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            var command = new MoveForward(transform, 1);
            commandInvoker.AddCommand(command);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            var command = new Rotator(transform, 90);
            commandInvoker.AddCommand(command);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            var command = new Rotator(transform, -90);
            commandInvoker.AddCommand(command);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            commandInvoker.ProcessAll();
        }
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            commandInvoker.Undo();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            commandInvoker.Process();
        }
    }
}
