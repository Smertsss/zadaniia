using UnityEngine;

public class ResourceObserver : MonoBehaviour
{
    private int[] resorce = new int[] { 0, 0, 0, 2 };

    private void OnEnable()
    {
        ResourceBank.OnResChanged += ResourceExchanger;
    }
    private void OnDisable()
    {
        ResourceBank.OnResChanged -= ResourceExchanger;
    }

    private void ResourceExchanger(ResType res, int val)
    {
        ResourceBank resObj = ResourceBank.resBankObj; 
        int resType = (int)res;
        resorce[resType] = resObj.GetResource(res);

        if (res == ResType.Wood)
        {
            if (resorce[resType] >= 10)
            {
                resorce[resType] -= 10;
                resorce[resType + 1]++;

                resObj.ChangeResource(res, resorce[resType]);
                resObj.ChangeResource(res + 1, resorce[resType + 1]);
            }
        }
        else if (res == ResType.House)
        {
            if (resorce[resType] >= 5)
            {
                int random = Random.Range(1, 6);

                resorce[resType] -= random;
                resorce[resType + 1] = resObj.GetResource(ResType.Money);
                resorce[resType + 1] += random * 100;

                resObj.ChangeResource(res, resorce[resType]);
                resObj.ChangeResource(res + 1, resorce[resType + 1]);
            }
        }
        else if (res == ResType.Money)
        {
            if (resorce[resType] >= 1000)
            {
                int random = Random.Range(1, 5);

                resorce[resType] -= random * 250;
                resorce[resType + 1] = resObj.GetResource(ResType.Worker);
                resorce[resType + 1] += random;

                resObj.ChangeResource(res, resorce[resType]);
                resObj.ChangeResource(res + 1, resorce[resType + 1]);
            }
        }

    }
}
