using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum ResType
{
    Wood,
    House,
    Money,
    Worker
}

public class ResourceBank : MonoBehaviour
{
    [SerializeField] private float waitTime = 0.5f;
    public Dictionary<ResType, int> resDictionary = new Dictionary<ResType, int>();
    public static Action<ResType, int> OnResChanged;
    public static ResourceBank resBankObj;

    private void Awake()
    {
        if (resBankObj == null)
        {
            resBankObj = FindObjectOfType<ResourceBank>();
            if (resBankObj == null)
            {
                GameObject resBank = new GameObject("ResourceBank");
                resBank.AddComponent<ResourceBank>();
                resBankObj = resBank.GetComponent<ResourceBank>();
            }
        }
        resDictionary.Add(ResType.Worker, 2);
        resDictionary.Add(ResType.Wood, 0);
        resDictionary.Add(ResType.House, 0);
        resDictionary.Add(ResType.Money, 0);
    }

    void Start()
    {
        StartCoroutine(AddWood());
    }

    public void ChangeResource(ResType res, int val)
    {
        resDictionary[res] = val;
        OnResChanged?.Invoke(res, val);
    }
    public int GetResource(ResType res)
    {
        return resDictionary[res];
    }

    IEnumerator AddWood()
    {
        while (true)
        {
            resDictionary[ResType.Wood] += resDictionary[ResType.Worker];
            ChangeResource(ResType.Wood, resDictionary[ResType.Wood]);
            yield return new WaitForSeconds(waitTime);
        }
    }
}
