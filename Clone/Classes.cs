using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IClonable
{
    IClonable Clone();
}
public class Student : IClonable
{
    public string Name;
    public string SecondName;
    public int Age;
    public Group Group;

    public Student(string name, string secondName, int age, Group group)
    {
        Name = name;
        SecondName = secondName;
        Age = age;
        Group = group;
    }

    public IClonable Clone()
    {
        return new Student(Name, SecondName, Age, new Group(Group.Course, Group.Spec));
    }
}

public class Group
{
    public int Course;
    public string Spec;

    public Group(int course, string spec)
    {
        Course = course;
        Spec = spec;
    }
}