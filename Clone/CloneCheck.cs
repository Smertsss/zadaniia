using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneCheck : MonoBehaviour
{
    static Student student = new Student("Saha", "Mider", 5, new Group(2, "Diz"));
    Student student2 = (Student)student.Clone();

    private void Start()
    {
        student2.Group.Course++;
        Debug.Log(student.Group.Course);
        Debug.Log(student2.Group.Course);
    }
}
