using UnityEngine;
using System.IO;


public class Decor : MonoBehaviour
{
    [SerializeField] private string str;
    private void Start()
    {
        IDebugable debugable = new CustomDebuger();
        DebugA debugA = new DebugA(debugable);
        DebugB debugB = new DebugB(debugA);
        debugA.Show(str);
        debugB.Show(str);
    }
}



public interface IDebugable
{
    public void Show(string s);
}

public class CustomDebuger : IDebugable
{
    public void Show(string s)
    {
        Debug.Log(s);
    }
}

public abstract class CustomDebugerDecorator : IDebugable
{
    private IDebugable iDebugable;
    
    public CustomDebugerDecorator(IDebugable debugable)
    {
        iDebugable = debugable;
    }

    public void Show(string s)
    {
        if (iDebugable != null)
            iDebugable.Show(s);
    }
}

public class DebugA : CustomDebugerDecorator
{
    public DebugA(IDebugable debugable) : base(debugable) { }
    public new void Show(string s)
    {
        base.Show(s);
        Debug.Log(s.Length);
    }
}

public class DebugB : CustomDebugerDecorator
{
    public DebugB(IDebugable debugable) : base(debugable) { }
    public new void Show(string s)
    {
        base.Show(s);
        File.WriteAllText(@"D:\Dec.txt", s);
    }
}
