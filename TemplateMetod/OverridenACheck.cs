using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverridenACheck : MonoBehaviour
{
    [SerializeField] private string s;
    [SerializeField] private int a;
    [SerializeField] private int b;
    [SerializeField] private int c;
    private OverridenA overridedA = new OverridenA();
    void Start()
    {
        Debug.Log(overridedA.TemplateMethod(s, a, b, c));
    }
}
