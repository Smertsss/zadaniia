using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverridenBCheck : MonoBehaviour
{
    [SerializeField] private string s;
    [SerializeField] private int a;
    [SerializeField] private int b;
    [SerializeField] private int c;
    private OverridenB overridedB = new OverridenB();
    void Start()
    {
        Debug.Log(overridedB.TemplateMethod(s, a, b, c));
    }
}
