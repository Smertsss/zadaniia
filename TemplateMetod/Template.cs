using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class OverridenA : Template
    {
        public override int AbstractA(string s) => s.Length;

        public override int AbstractB() => 42;
    }


    public class OverridenB : Template
    {
        int sum;
        public override int AbstractA(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (s.Contains("a") || s.Contains("e") || s.Contains("i") || s.Contains("o") || s.Contains("u") || s.Contains("y"))
                    sum++;
            }
            return sum;
        }

        public override int AbstractB() => Random.Range(1, 3);
        public override int VirtualA(int a, int b) => a * b;
        public override int VirtualB(int c) => c % 7;
    }
public abstract class Template
{
    public abstract int AbstractA(string s);
    public abstract int AbstractB();

    public virtual int VirtualA(int a, int b) => a + b;
    public virtual int VirtualB(int c) => 
        (int)Mathf.Pow(c, 2);

    public int TemplateMethod(string s, int a, int b, int c) =>
        ((int)Mathf.Pow(AbstractA(s), AbstractB()) + VirtualA(a, b) * VirtualB(c));
}