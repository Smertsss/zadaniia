using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



public class Strategy : MonoBehaviour
{
    [SerializeField] private float mSpeed = 100;
    [SerializeField] private float rSpeed = 100;
    [SerializeField] private float repeat = 10;
    private IStrategy Move = new MoveForward();
    private IStrategy Rotate = new Rotate();
    private IStrategy Emmit = new Emmit();
    public bool canMove;
    public bool canRotate;
    public bool canEmmit;

    private void ExecuteMoveAlgorithm()
    {
        Move.Perform(transform, mSpeed);
    }
    private void ExecuteRotateAlgorithm()
    {
        Rotate.Perform(transform, rSpeed);
    }
    private void ExecuteEmmitAlgorithm()
    {
        Emmit.Perform(transform, repeat);
    }

    public void Update()
    {
        if (canMove)
        {
            ExecuteMoveAlgorithm();
        }
        if (canRotate)
        {
            ExecuteRotateAlgorithm();
        }
        if (canEmmit)
        {
            ExecuteEmmitAlgorithm();
        }
    }
}



interface IStrategy
{
    void Perform(Transform transform, float col);
}

public class MoveForward : IStrategy
{
    public void Perform(Transform transform, float col)
    {
        transform.Translate(col * Time.deltaTime * transform.forward);
    }
}

public class Rotate : IStrategy
{
    public void Perform(Transform transform, float col)
    {
        transform.Rotate(col * Time.deltaTime * transform.right);
    }
}

public class Emmit : IStrategy
{
    public void Perform(Transform transform, float col)
    {
        ParticleSystem particle = transform.GetComponent<ParticleSystem>();
        particle.Emit((int)col);
    }
}
