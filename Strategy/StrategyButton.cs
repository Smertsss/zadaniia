using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrategyButton : MonoBehaviour
{
    public Strategy strategy;

    public void Move()
    {
        if (strategy.canMove) { 
        strategy.canMove = false; }
        else
            strategy.canMove = true;
    }
    public void Rotate()
    {
        if (strategy.canRotate)
            strategy.canRotate = false;
        else
            strategy.canRotate = true;
    }
    public void Emmit()
    {
        if (strategy.canRotate)
            strategy.canEmmit = false;
        else
            strategy.canEmmit = true;
    }
}
